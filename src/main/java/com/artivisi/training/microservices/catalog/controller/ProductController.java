package com.artivisi.training.microservices.catalog.controller;

import com.artivisi.training.microservices.catalog.dao.ProductDao;
import com.artivisi.training.microservices.catalog.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/product")
@Slf4j
public class ProductController {

    @Autowired private ProductDao productDao;

    @GetMapping("/{id}")
    public Product findById(@PathVariable String id, HttpServletRequest request){
        log.info("Mencari product dengan id {}", id);
        Product p = productDao.findById(id)
                .orElse(null);

        if (p != null) {
            // ambil informasi hostname aplikasi
            String hostname = request.getServerName();
            p.setBackendSource(hostname);
        }

        return p;
    }

    @GetMapping("/")
    public Page<Product> findAll(Pageable pageable) {
        return productDao.findAll(pageable);
    }
}
